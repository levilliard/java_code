
/*

	create generic DAO
	@levilliard@gmail.com

*/
package com.levilliard.javacode.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.obliqsystem.drobliq.util.HibernateUtil;

public class GenericDAO {

	//private Session session = HibernateUtil
	public <T> boolean addObject(T obj) {
		Transaction trns = null;
                boolean result = false;
		Session session = HibernateUtil.getSessionFactory().openSession();
                
		try {
			session.save(obj);
			trns = session.beginTransaction();
			session.getTransaction().commit();
            result = true;
		} catch (RuntimeException e) {
			if (trns != null) {
                            trns.rollback();
			}
                        result = false;
			e.printStackTrace();
		} finally {
			session.close();
		}
        
        return result;
	}

	public boolean deleteObject(String objType, int id) {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
        boolean result = false;
                
		try {
			
			/*
				delete from objType where objType_id = ?
				with a databse table name = objType and id = objType_id

			*/
			trns = session.beginTransaction();
			Query query = session.createQuery("delete from " + objType + " where " + objType + "_id = :id ");
			query.setParameter("id", id);
			query.executeUpdate(); 
			session.getTransaction().commit();            
			result = true;
		} catch (RuntimeException e) {
			e.printStackTrace();
            result = false;
		} finally {
			session.close();
		}
                
        return result;
	}

	public <T> boolean updateObject(T obj) {
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
                boolean result = false;

		try {
			session.update(obj);
			trns = session.beginTransaction();
			session.getTransaction().commit();
                        result = true;
                        
		} catch (RuntimeException e) {
			if (trns != null) {
				trns.rollback();
			}
                        result = false;
			e.printStackTrace();
		} finally {
			session.close();
		}
            return result;
	}
 
    /*
    	query must contain the name of the generic type T
    	ex: query = "from User"
    */
	public <T> List<T> getAllObject(String query) {
		List<T> objs = new ArrayList<T>();
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			trns = session.beginTransaction();
			objs = session.createQuery(query).list();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return objs;
	}

    /*
        query must contain the name of the generic type T
	    ex: queryStr = "from User where id = :id";
	*/
	public <T> T getObjectById(String queryStr, int userId) {
		T obj = null;
		Transaction trns = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			trns = session.beginTransaction();
			Query query = session.createQuery(queryStr);
			query.setInteger("id", userId);
			obj = (T) query.uniqueResult();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}

		return obj;
	}
}
